from treelib import Tree


class Serialization:
    def __init__(self, d: dict):
        [
            setattr(self, key, d[key]) for key in d
        ]


class TreeStore:
    def __init__(self, sr: list[Serialization]):
        tree = Tree()
        tree.create_node(sr[0].id, sr[0].id)
        [tree.create_node(i.id, i.id, parent=i.parent, data=i.type) for i in sr[1:]]
        self.tree = tree

    def get_all(self) -> Tree: return self.tree.nodes

    def get_item(self, id: int) -> Tree: return self.tree.get_node(id)

    def get_children(self, id: int) -> Tree: return self.tree.children(id)

    def get_all_parents(self, id: int) -> list[Tree]:
        return [self.tree.parent(i) for i in self.tree.rsearch(id)]


if __name__ == "__main__":
    items = [
        {"id": 1, "parent": "root"},
        {"id": 2, "parent": 1, "type": "test"},
        {"id": 3, "parent": 1, "type": "test"},
        {"id": 4, "parent": 2, "type": "test"},
        {"id": 5, "parent": 2, "type": "test"},
        {"id": 6, "parent": 2, "type": "test"},
        {"id": 7, "parent": 4, "type": None},
        {"id": 8, "parent": 4, "type": None}
    ]

    ts = TreeStore([Serialization(i) for i in items])

    print(f'get_all: {ts.get_all()}\n')
    print(f'get_item: {ts.get_item(7)}\n')
    print(f'get_children: {ts.get_children(4)}\n')
    print(f'get_children: {ts.get_children(5)}\n')
    print(f'get_all_parents: {ts.get_all_parents(7)}\n')
